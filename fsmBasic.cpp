#include <iostream>
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/state_machine_def.hpp>
#include <chrono>
#include <thread>

namespace msm = boost::msm;
namespace mpl = boost::mpl;

void printChords(std::vector<std::string> chords)
{
    std::cout << "playing || ";
    for (std::string aChord : chords)
    {
      std::cout << aChord << " | ";
    }
    std::cout << " ||";
}

// ***** Events *****
struct DrummerCue 
{
  
  DrummerCue(int bpm) 
    : _tempo(bpm)
  {
    std::cout << "drummer lifting hands to start counting!\n" << std::endl;
  }

  int _tempo;
};

struct VerseCue
{
  VerseCue(std::vector<std::string> chords)
    : chordProgression(chords)
  { }

  std::vector<std::string> chordProgression;
};

struct ChorusCue {};
struct SoloCue
{
  SoloCue(std::string k)
    : key(k)
  { }
    
  std::string key;
};

struct TomatoThrown {};

struct RoboMusician_ : public msm::front::state_machine_def<RoboMusician_>
{
  // ***** STATE MACHINE ENTRY AND EXIT *******
  template<class Event, class FSM>
  void on_entry(Event const&, FSM&)
  {
    std::cout << "*grabs guitar*...." << std::endl;
    std::cout << "*tunes that damn third string*...." << std::endl;
    std::cout << "*takes a sip of Medalla*" << std::endl;
    std::cout << std::endl;
  }

  template<class Event, class FSM>
  void on_exit(Event const&, FSM&)
  {
    std::cout << "*turns volume to 0*\n" << std::endl;
  }

  // ******* STATES ******* //
  struct Awaiting : public msm::front::state<>
  {
    template <class Event, class FSM>
    void on_entry(Event const&, FSM&)
    {
      std::cout << "awaiting cue from drummer...\n" << std::endl;
    }

    template <class Event, class FSM>
    void on_exit(Event const&, FSM&)
    {
      std::cout << "exiting awaiting\n" << std::endl;
    }
  };


  // THIS STATE IS ALSO A STATE MACHINE
  struct Playing_ : public msm::front::state_machine_def<Playing_>
  {
    int _tempo;

    template<class Event, class FSM>
    void on_entry(Event const&, FSM&) { std::cout << "entering Playing...\n" << std::endl; }
    template<class Event, class FSM>
    void on_exit(Event const&, FSM&) { std::cout << "leaving Playing...\n" << std::endl; }

    // **** STATES ****
    struct CatchingBeat : public msm::front::state<>
    {
      template<class Event, class FSM>
      void on_entry(Event const& ev, FSM& fsm)
      {
        std::cout << "entering hopping on the beat..." << std::endl;
        std::cout << "And a 1..." << std::endl;
        usleep(1000000);
        std::cout << "and a 2..." << std::endl;
        usleep(1000000);
        std::cout << "and a\n1..." << std::endl;
        usleep(500000);
        std::cout << "2..." << std::endl;
        usleep(500000);
        std::cout << "3..." << std::endl;
        usleep(500000);
        std::cout << "4...\n" << std::endl;
        usleep(500000);
      }

      template<class Event, class FSM>
      void on_exit(Event const&, FSM&)
      {
        std::cout << "leaving hopping on the beat...\n" << std::endl;
      }
    };

    struct PlayingVerse : public msm::front::state<>
    {
      template<class Event, class FSM>
      void on_entry(Event const& ev, FSM& fsm)
      {
        std::cout << "entering playing verse...\n" << std::endl;
      }

      template<class Event, class FSM>
      void on_exit(Event const&, FSM&)
      {
        std::cout << "leaving playing verse...\n" << std::endl;
      }
    };
    
    struct Soloing : public msm::front::state<>
    {
      template<class Event, class FSM>
      void on_entry(Event const& ev, FSM& fsm)
      {
        std::cout << "entering playing verse...\n" << std::endl;
      }

      template<class Event, class FSM>
      void on_exit(Event const&, FSM&)
      {
        std::cout << "leaving playing verse...\n" << std::endl;
      }
    };

    typedef CatchingBeat initial_state; //OBLIGATORY?

    // **** ACTIONS ****
    void listenToDrummerTempo(DrummerCue const& ev)
    {
      _tempo = ev._tempo;
      sleep(4);
    }

    void playVerse(VerseCue const& ev)
    {
      std::cout << "playing: ||: ";
      for (std::string aChord : ev.chordProgression)
      {
        std::cout << aChord << " | ";
      }
      std::cout << " :||" << std::endl;
      sleep(4);
    }

    void printSome(VerseCue const& ev)
    {
      std::cout << "heyooooo" << std::endl;
    }

    void playSolo(SoloCue const& ev)
    {
      std::cout << "soloing in key: " << ev.key << std::endl << std::endl;
    }
 
    // ***** TRANSITION TABLE *****
    // TODO CHECK VECTOR4. WHATSUP WITH THAT 4?
    struct transition_table : mpl::vector< 
      //      StartState     Event         NextSt        Action               Guard
      //    +--------------+-------------+-------------+---------------------+----------------------+
      a_row <  CatchingBeat,  VerseCue,     PlayingVerse,  &Playing_::playVerse                        >,
      a_row <  PlayingVerse,  SoloCue,      Soloing,       &Playing_::playSolo                         >
      >
    {};
    
    template <class Event, class FSM>
    void no_transition(Event const& ev, FSM &, int state)
    {
      std::cout << "No transition from state " << state 
                << " on event " << typeid(ev).name() << std::endl << std::endl;
    }
  }; //end front end

  typedef msm::back::state_machine<Playing_> Playing;
  
  typedef Awaiting initial_state;

  // **** ACTIONS FOR MAIN STATE MACHINE ****
  void startPlaying(DrummerCue const&) { std::cout << "Drummer is cueing! start playing!\n" << std::endl; }
  void haltPlaying(TomatoThrown const&) { std::cout << "Tomatoes flying! Stop!\n" << std::endl; }
  
  struct transition_table : mpl::vector<
    //      StartState     Event         NextSt        Action                      Guard
    //    +--------------+-------------+-------------+----------------------------+----------------------+
    a_row < Awaiting,     DrummerCue,   Playing,      &RoboMusician_::startPlaying                        >,
    a_row < Playing,      TomatoThrown,  Awaiting,     &RoboMusician_::haltPlaying                         >
  > {};

  template <class Event, class FSM>
  void no_transition(Event const& ev, FSM&, int state)
  {
      std::cout << "No transition from state " << state 
                << " on event " << typeid(ev).name() << std::endl << std::endl;
  }

};

typedef msm::back::state_machine<RoboMusician_> RoboMusician;

static char const* const state_names[] = {"Awaiting", "Playing"};

void printState(RoboMusician const& robo)
{
  std::cout << "->" << state_names[robo.current_state()[0]] << std::endl;
}

int main()
{
  RoboMusician eddie; //Van Halen duh
  eddie.start();

  usleep(2000000);
  eddie.process_event(DrummerCue(120)); // Construct cue event and process

  std::vector<std::string> hot4TeacherChords = {"Am", "C5", "Eb5", "D5"};
  eddie.process_event(VerseCue(hot4TeacherChords));
  eddie.stop();

  return 0;
}